# $value = hiera('norootlogin')

node 'puppetnode' {

	class {'mod_ssh':}
	notify {"value: ${value}":}
	$value = hiera('permitrootlogin')

}

node 'puppetmaster' {

	class {'mod_ssh':}
	notify {"value: ${value}":}

}

node 'vagrant-2012-r2' {

	class {'profiles':}

}
#
