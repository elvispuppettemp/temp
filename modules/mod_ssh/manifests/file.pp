#/etc/puppetlabs/code/environments/temp/modules/mod_ssh/manifests/file.pp
	

	class mod_ssh::file {
               #value = yes
		file { '/etc/ssh/sshd_config':
		  ensure  => 'file',
		  group   => 'root',
		  mode    => '0644',
		  content => template('mod_ssh/sshd_config.erb'),
		  owner   => 'root',
	}
}
