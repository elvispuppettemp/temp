#/etc/puppetlabs/code/environments/temp/modules/mod_ssh/manifests/service.pp

	class mod_ssh::service {

		service { 'sshd.service':
  		ensure => 'running',
  		enable => 'true',
		}
	}
